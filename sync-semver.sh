#!/usr/bin/env bash

applicationVersion=$1

if [ -z "$applicationVersion" ]
then
    applicationVersion=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
fi

echo "applicationVersion = $applicationVersion"

sed -i "s|tag: .*|tag: \"$applicationVersion\"|g" ./chart/values.yaml
echo "(updated)  chart/values.yaml < tag: \"$applicationVersion\""

sed -i "s|version: .*|version: \"$applicationVersion\"|g" ./chart/Chart.yaml
echo "(updated)  chart/Chart.yaml < version: \"$applicationVersion\""
