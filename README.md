# shop


## deployment

This resource is intended to be called up by the flux repository and deployed in a cluster.
However, it can be started locally via
```bash
./mvnw quarkus:dev
```

In the latter case, associations are calculated internally using a brute force algorithm.
In normal deployment, the shop is meant to call upon an outsourced association service using HTTP.
Via Unleash Toggles this service is made exchangable for the admins.
More on this in the Flux README.

# endpoints

Offers two main json endpoints on port 8080.
```bash
/shop/associations
```
This accepts a list of items defined by name.
It returns the calculated associations as a new list of items.


```bash
/shop/checkout
```
commit the current cart, again a list of items, into the database.
