package org.acme;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import org.junit.jupiter.api.Test;

public class ShopTest {

    @Test
    public void testPowerSet() {
        Shop shop = new Shop();
        assertEquals(6, shop.getItems().size());
        assertEquals(0, shop.getTransactions().size());

        var permutations = shop.getPowerSet(Arrays.asList(
                new Item("Beer"),
                new Item("Milk"),
                new Item("Coke")));
        assertEquals(7, permutations.size());

        var expected = Arrays.asList(
                Arrays.asList(new Item("Beer")),
                Arrays.asList(new Item("Milk")),
                Arrays.asList(new Item("Coke")),
                Arrays.asList(new Item("Beer"), new Item("Milk")),
                Arrays.asList(new Item("Beer"), new Item("Coke")),
                Arrays.asList(new Item("Milk"), new Item("Coke")),
                Arrays.asList(new Item("Beer"), new Item("Milk"), new Item("Coke")));

        assertEquals(expected.size(), permutations.size());
        for (var sublist : expected) {
            boolean found = false;
            for (var permutation : permutations) {
                if (permutation.containsAll(sublist)) {
                    found = true;
                    break;
                }
            }
            assertEquals(true, found);
        }
    }

    @Test
    public void testGetAllCartsFromList() {
        Shop shop = new Shop();
        assertEquals(6, shop.getItems().size());
        assertEquals(0, shop.getTransactions().size());
        shop.addCart(Arrays.asList(new Item("Beer"), new Item("Milk")));
        shop.addCart(Arrays.asList(new Item("Beer"), new Item("Coke")));
        shop.addCart(Arrays.asList(new Item("Milk"), new Item("Coke")));
        shop.addCart(Arrays.asList(new Item("Beer"), new Item("Milk"), new Item("Coke")));
        shop.addCart(Arrays.asList(new Item("Beer")));
        shop.addCart(Arrays.asList(new Item("Milk")));
        shop.addCart(Arrays.asList(new Item("Coke")));
        shop.addCart(Arrays.asList(new Item("Eggs")));
        assertEquals(8, shop.getTransactions().size());


        // put all of thos carts containing "Beer" into a list
        var beerCarts = shop.getAllCartsFromList(Arrays.asList(new Item("Beer")));
        assertEquals(4, beerCarts.size());
        assertEquals(true, beerCarts.contains(Arrays.asList(new Item("Beer"), new Item("Milk"))));
        assertEquals(true, beerCarts.contains(Arrays.asList(new Item("Beer"), new Item("Coke"))));
        assertEquals(true, beerCarts.contains(Arrays.asList(new Item("Beer"), new Item("Milk"), new Item("Coke"))));
        assertEquals(true, beerCarts.contains(Arrays.asList(new Item("Beer"))));

        // do the same for Milk
        var milkCarts = shop.getAllCartsFromList(Arrays.asList(new Item("Milk")));
        assertEquals(4, milkCarts.size());
        assertEquals(true, milkCarts.contains(Arrays.asList(new Item("Beer"), new Item("Milk"))));
        assertEquals(true, milkCarts.contains(Arrays.asList(new Item("Milk"), new Item("Coke"))));
        assertEquals(true, milkCarts.contains(Arrays.asList(new Item("Beer"), new Item("Milk"), new Item("Coke"))));
        assertEquals(true, milkCarts.contains(Arrays.asList(new Item("Milk"))));

        // and the same for Beer and Milk
        var beerMilkCarts = shop.getAllCartsFromList(Arrays.asList(new Item("Beer"), new Item("Milk")));
        assertEquals(2, beerMilkCarts.size());
        assertEquals(true, beerMilkCarts.contains(Arrays.asList(new Item("Beer"), new Item("Milk"))));
        assertEquals(true, beerMilkCarts.contains(Arrays.asList(new Item("Beer"), new Item("Milk"), new Item("Coke"))));
    }

    @Test
    public void testFrequentItemSets() {
        // prepare the shop with some transactions
        Shop shop = new Shop();
        assertEquals(6, shop.getItems().size());
        assertEquals(0, shop.getTransactions().size());
        shop.addCart(Arrays.asList(new Item("Bread"), new Item("Milk")));
        shop.addCart(Arrays.asList(new Item("Bread"), new Item("Diaper"), new Item("Beer"), new Item("Eggs")));
        shop.addCart(Arrays.asList(new Item("Milk"), new Item("Diaper"), new Item("Beer"), new Item("Coke")));
        shop.addCart(Arrays.asList(new Item("Bread"), new Item("Milk"), new Item("Diaper"), new Item("Beer")));
        shop.addCart(Arrays.asList(new Item("Bread"), new Item("Milk"), new Item("Diaper"), new Item("Coke")));
        assertEquals(5, shop.getTransactions().size());

        var cart = Arrays.asList(new Item("Beer"));
        // make a list tof all item but beer
        var items = Arrays.asList(new Item("Bread"), new Item("Milk"), new Item("Diaper"), new Item("Coke"), new Item("Eggs"));
        
        /************************************************************/
        /************************************************************/
        // this part is kind of interchangable by the smater
        // implementation of the getFrequentItemSets method
        var pSet = shop.getPowerSet(items);
        var frequentItemSets = shop.getFrequentItemSets(pSet, cart);
        assertEquals(pSet.size(), frequentItemSets.size());
        /************************************************************/
        /************************************************************/


        // tests if all calculated sets have the right support and confidence
        double oneThird = 0.33;
        double twoThirds = 0.67;
        for (var itemSet : frequentItemSets) {
            // Y = Bread -> support of 3/5 and confidence of 3/4
            // Y = Milk -> support of 3/5 and confidence of 3/4
            // Y = Diaper -> support of 3/5 and confidence of 3/4
            // Y = Coke -> support of 2/5 and confidence of 2/3
            // Y = Eggs -> support of 1/5 and confidence of 1/2
            if (itemSet.getItem1().size() == 1) {
                if (itemSet.getItem1().get(0).equals(new Item("Bread"))) {
                    assertEquals(2.0 / 5.0, itemSet.getItem2());
                    assertEquals(twoThirds, itemSet.getItem3());
                }
                if (itemSet.getItem1().get(0).equals(new Item("Milk"))) {
                    assertEquals(2.0 / 5.0, itemSet.getItem2());
                    assertEquals(twoThirds, itemSet.getItem3());
                }
                if (itemSet.getItem1().get(0).equals(new Item("Diaper"))) {
                    assertEquals(3.0 / 5.0, itemSet.getItem2());
                    assertEquals(1.0 / 1.0, itemSet.getItem3());
                }
                if (itemSet.getItem1().get(0).equals(new Item("Coke"))) {
                    assertEquals(1.0 / 5.0, itemSet.getItem2());
                    assertEquals(oneThird, itemSet.getItem3());
                }
                if (itemSet.getItem1().get(0).equals(new Item("Eggs"))) {
                    assertEquals(1.0 / 5.0, itemSet.getItem2());
                    assertEquals(oneThird, itemSet.getItem3());
                }
            }
            // Y = Bread, Eggs -> support of 1/5 and confidence of 1/3
            // Y = Bread, Coke -> support of 0/5 and confidence of 0/3
            // Y = Bread, Diaper -> support of 2/5 and confidence of 2/3
            // Y = Bread, Milk -> support of 1/5 and confidence of 1/3
            // Y = Milk, Eggs -> support of 0/5 and confidence of 0/3
            // Y = Milk, Coke -> support of 1/5 and confidence of 1/3
            // Y = Milk, Diaper -> support of 2/5 and confidence of 2/3
            // Y = Diaper, Eggs -> support of 1/5 and confidence of 1/3
            // Y = Diaper, Coke -> support of 1/5 and confidence of 1/3
            // Y = Coke, Eggs -> support of 0/5 and confidence of 0/3
            if (itemSet.getItem1().size() == 2) {
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(1.0 / 5.0, itemSet.getItem2());
                    assertEquals(oneThird, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Coke"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Diaper"))) {
                    assertEquals(2.0 / 5.0, itemSet.getItem2());
                    assertEquals(twoThirds, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Milk"))) {
                    assertEquals(1.0 / 5.0, itemSet.getItem2());
                    assertEquals(oneThird, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Coke"))) {
                    assertEquals(1.0 / 5.0, itemSet.getItem2());
                    assertEquals(oneThird, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Diaper"))) {
                    assertEquals(2.0 / 5.0, itemSet.getItem2());
                    assertEquals(twoThirds, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Diaper")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(1.0 / 5.0, itemSet.getItem2());
                    assertEquals(oneThird, itemSet.getItem3());
                }
            }
            // Y = Bread, Coke, Eggs -> support of 0/5 and confidence of 0/3
            // Y = Bread, Diaper, Eggs -> support of 1/5 and confidence of 1/3
            // Y = Bread, Diaper, Coke -> support of 0/5 and confidence of 0/3
            // Y = Bread, Milk, Eggs -> support of 0/5 and confidence of 0/3
            // Y = Bread, Milk, Coke -> support of 0/5 and confidence of 0/3
            // Y = Bread, Milk, Diaper -> support of 1/5 and confidence of 1/3
            // Y = Milk, Coke, Eggs -> support of 0/5 and confidence of 0/3
            // Y = Milk, Diaper, Eggs -> support of 0/5 and confidence of 0/3
            // Y = Milk, Diaper, Coke -> support of 1/5 and confidence of 1/3
            // Y = Diaper, Coke, Eggs -> support of 0/5 and confidence of 0/3
            if (itemSet.getItem1().size() == 3) {
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Coke")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Diaper")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(1.0 / 5.0, itemSet.getItem2());
                    assertEquals(oneThird, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Diaper")) && itemSet.getItem1().contains(new Item("Coke"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Coke"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Diaper"))) {
                    assertEquals(1.0 / 5.0, itemSet.getItem2());
                    assertEquals(oneThird, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Coke")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
            }
            // Y = Bread, Diaper, Coke, Eggs -> support of 0/5 and confidence of 0/3
            // Y = Bread, Milk, Coke, Eggs -> support of 0/5 and confidence of 0/3
            // Y = Bread, Milk, Diaper, Eggs -> support of 0/5 and confidence of 0/3
            // Y = Bread, Milk, Diaper, Coke -> support of 0/5 and confidence of 0/3
            // Y = Milk, Diaper, Coke, Eggs -> support of 0/5 and confidence of 0/3
            if (itemSet.getItem1().size() == 4) {
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Coke")) && itemSet.getItem1().contains(new Item("Diaper")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Coke")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Diaper")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Diaper")) && itemSet.getItem1().contains(new Item("Coke"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
            }
            // Y = Bread, Milk, Diaper, Coke, Eggs -> support of 0/5 and confidence of 0/3
            if (itemSet.getItem1().size() == 5) {
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Diaper")) && itemSet.getItem1().contains(new Item("Coke")) && itemSet.getItem1().contains(new Item("Eggs"))) {
                    assertEquals(0.0 / 5.0, itemSet.getItem2());
                    assertEquals(0.0 / 3.0, itemSet.getItem3());
                }
            }
        }

        // test the pruning
        shop.setConfidenceAndSupport(0.5, 0.3);
        var pruned = shop.pruneItemSets(frequentItemSets);
        assertEquals(5, pruned.size());
        // check if all the expected item sets are in the pruned list
        int found = 0;
        for (var itemSet : pruned) {
            if (itemSet.getItem1().size() == 1) {
                if (itemSet.getItem1().get(0).equals(new Item("Bread"))) {
                    found++;
                }
                if (itemSet.getItem1().get(0).equals(new Item("Milk"))) {
                    found++;
                }
                if (itemSet.getItem1().get(0).equals(new Item("Diaper"))) {
                    found++;
                }
            }
            if (itemSet.getItem1().size() == 2) {
                if (itemSet.getItem1().contains(new Item("Bread")) && itemSet.getItem1().contains(new Item("Diaper"))) {
                    found++;
                }
                if (itemSet.getItem1().contains(new Item("Milk")) && itemSet.getItem1().contains(new Item("Diaper"))) {
                    found++;
                }
            }
        }
        assertEquals(5, found);

        // test the generation of the best candidate
        // the best candidate should be the one with size 2
        // but do not check for confidence because both have the same
        var best = shop.generateBestCandidate(pruned);
        assertEquals(2, best.size());

        // test the generation of the best candidate
        var result = shop.getAssociations(cart);
        assertEquals(2, result.size());
        // assert that the result is the same as best
        assertEquals(best.size(), result.size());
        for (var item : best) {
            assertEquals(true, result.contains(item));
        }
    }

    @Test
    public void TestAssociationEdgeCases() {
        // a cart containing every item should return an empty list
        Shop shop = new Shop();
        assertEquals(6, shop.getItems().size());
        assertEquals(0, shop.getTransactions().size());
        var cart = Arrays.asList(new Item("Beer"), new Item("Milk"), new Item("Diaper"), new Item("Coke"), new Item("Eggs"), new Item("Bread"));
        shop.addCart(cart);
        assertEquals(1, shop.getTransactions().size());
        var result = shop.getAssociations(cart);
        assertEquals(0, result.size());
    }
}

