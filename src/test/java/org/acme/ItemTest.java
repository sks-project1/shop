package org.acme;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemTest {

    @Test
    public void testItem() {
        Item item = new Item("Beer");
        assertEquals("Beer", item.getName());
    }

    @Test
    public void testShop() {
        Shop shop = new Shop();
        assertEquals(6, shop.getItems().size());
        assertEquals(0, shop.getTransactions().size());
    }
}