package org.acme;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.stream.IntStream;

import io.quarkus.logging.Log;
import io.quarkus.runtime.Startup;
import io.smallrye.mutiny.tuples.Tuple3;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class Shop {

    private List<Item> items = Arrays.asList(
            new Item("Beer"),
            new Item("Diaper"),
            new Item("Milk"),
            new Item("Bread"),
            new Item("Eggs"),
            new Item("Coke"));

    private List<List<Item>> transactions = new ArrayList<>();

    private Random random = new Random();

    double minConfidence = 0.3;
    double minSupport = 0.2;
    // setter for confidence and support
    public void setConfidenceAndSupport(double confidence, double support) {
        this.minConfidence = confidence;
        this.minSupport = support;
    }


    public List<Item> getItems() {
        return items;
    }

    public List<List<Item>> getTransactions() {
        return transactions;
    }

    public void addCart(List<Item> cart) {
        transactions.add(cart);
    }

    // This method runs on startup and creates 1000 random carts.
    @Startup
    void init() {
        IntStream.range(0, 1000).forEach(
                i -> {
                    List<Item> transaction = new ArrayList<>();
                    if (random.nextBoolean()) {
                        transaction.add(items.get(0));
                    }
                    if (random.nextBoolean()) {
                        transaction.add(items.get(1));
                    }
                    if (random.nextBoolean()) {
                        transaction.add(items.get(2));
                    }
                    if (random.nextBoolean()) {
                        transaction.add(items.get(3));
                    }
                    if (random.nextBoolean()) {
                        transaction.add(items.get(4));
                    }
                    if (random.nextBoolean()) {
                        transaction.add(items.get(5));
                    }
                    transactions.add(transaction);
                });

        Log.info("Created " + transactions.size() + " carts");

    }

    // This method returns the power set of a given list.
    public <T> List<List<T>> getPowerSet(List<T> candidates) {
        List<List<T>> sets = new ArrayList<>();
        for (T element : candidates) {
            for (ListIterator<List<T>> setsIterator = sets.listIterator(); setsIterator.hasNext();) {
                List<T> newSet = new ArrayList<>(setsIterator.next());
                newSet.add(element);
                setsIterator.add(newSet);
            }
            sets.add(new ArrayList<>(Arrays.asList(element)));
        }
        return sets;
    }

    // This method returns all transactions that contain all items in the given list.
    // It returns a list of lists, where each list is a transaction.
    public List<List<Item>> getAllCartsFromList(List<Item> items) {
        List<List<Item>> transactions = new ArrayList<>();
        for (List<Item> transaction : this.transactions) {
            if (transaction.containsAll(items)) {
                transactions.add(transaction);
            }
        }
        return transactions;
    }

    // This method computes the frequent item sets for a given list of items.
    // It returns a list of tuples, where each tuple contains a frequent item set,
    // the relative support and the relative confidence.
    // The results are not pruned in any way.
    public List<Tuple3<List<Item>, Double, Double>> getFrequentItemSets(
            List<List<Item>> permutations,
            List<Item> shoppingChart) {
        // make a list of all transactions containing the shopping cart
        List<List<Item>> X_transactions = getAllCartsFromList(shoppingChart);

        List<Tuple3<List<Item>, Double, Double>> frequentItemsets = new ArrayList<>();
        for (List<Item> permutation : permutations) {
            int support = 0;
            for (List<Item> transaction : X_transactions) {
                if (transaction.containsAll(permutation)) {
                    support++;
                }
            }
            // calculate relative support and confidence
            double relativeSupport = Math.round(((double) support / this.transactions.size()) * 100.0) / 100.0;
            double relativeConfidence = Math.round(((double) support / X_transactions.size()) * 100.0) / 100.0;
            frequentItemsets.add(Tuple3.of(permutation, relativeSupport, relativeConfidence));
        }
        return frequentItemsets;
    }

    // This method prunes the frequent item sets depending on minSupport and minConfidence.
    // It returns a list of tuples, where each tuple contains a frequent item set,
    // the relative support and the relative confidence.
    public List<Tuple3<List<Item>, Double, Double>> pruneItemSets(
            List<Tuple3<List<Item>, Double, Double>> frequentItemsets) {
        Tuple3<List<Item>, Double, Double> mostConfident = null;
        List<Tuple3<List<Item>, Double, Double>> prunedFrequentItemsets = new ArrayList<>();
        for (Tuple3<List<Item>, Double, Double> frequentItemset : frequentItemsets) {
            // prune depending on minSupport and minConfidence
            if (frequentItemset.getItem2() >= this.minSupport && frequentItemset.getItem3() >= this.minConfidence) {
                prunedFrequentItemsets.add(frequentItemset);
            } else if (prunedFrequentItemsets.size() == 0) {
                // set mostConfident to permutation if no frequentItemsets have been found yet
                if (mostConfident == null) {
                    mostConfident = frequentItemset;
                } else if (frequentItemset.getItem3() > mostConfident.getItem3()) {
                    mostConfident = frequentItemset;
                }
            }
        }
        // if no frequentItemsets have been found, return mostConfident
        if (prunedFrequentItemsets.size() == 0) {
            prunedFrequentItemsets.add(mostConfident);
        }
        return prunedFrequentItemsets;
    }

    // This method computes the association rules for a given list of items.
    // It returns the longest frequent itemset with the highest confidence.
    public List<Item> generateBestCandidate(List<Tuple3<List<Item>, Double, Double>> frequentItemsets) {
        // sort frequentItemsets by size and within size by (reversed) confidence
        frequentItemsets.sort((a, b) -> {
            // if size is equal, sort by confidence, where lower confidence is better
            if (a.getItem1().size() == b.getItem1().size()) {
                return Double.compare(a.getItem3(), b.getItem3());
            }
            return a.getItem1().size() - b.getItem1().size();
        });
        // return the longest frequent itemset with the highest confidence
        return frequentItemsets.get(frequentItemsets.size() - 1).getItem1();
    }

    // This method computes the association rules for a given list of items.
    // It returns the longest frequent itemset.
    public List<Item> getAssociations(List<Item> shoppingChart) {
        // if every item in the shopping chart is already in the candidates, return empty list
        if (shoppingChart.size() == items.size()) {
            return new ArrayList<>();
        }

        // get all items that are not in the shopping chart
        List<Item> candidates = new ArrayList<>();
        items.forEach(item -> {
            if (!shoppingChart.contains(item)) {
                candidates.add(item);
            }
        });

        // create the power set of candidates
        List<List<Item>> powerSet = getPowerSet(candidates);
        // get freqency for each set
        List<Tuple3<List<Item>, Double, Double>> frequentItemsets = getFrequentItemSets(powerSet, shoppingChart);
        // get most confident itemsets depending on minSupport and minConfidence
        frequentItemsets = pruneItemSets(frequentItemsets);

        return generateBestCandidate(frequentItemsets);
    }
}
