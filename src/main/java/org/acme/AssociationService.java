package org.acme;

import java.util.List;

import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@Path("/api")
@RegisterRestClient
public interface AssociationService {
    @POST
    @Path("/associations")
    List<Item> getAssociations(List<Item> shoppingCart);

    @POST
    @Path("/checkout")
    void checkout(List<Item> cart);
}
