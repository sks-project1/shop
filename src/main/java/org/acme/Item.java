package org.acme;

import java.util.List;

import io.quarkus.logging.Log;

public class Item {
    
    public String name;

    public Item(){
    }

    public Item(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean equals(Object obj){
        if(obj instanceof Item){
            Item item = (Item) obj;
            return item.getName().equals(this.getName());
        }
        return false;
    }

    // log a list of items
    public static void logItems(String prefix, List<Item> items) {
        StringBuilder sb = new StringBuilder();
        sb.append(prefix).append(": ");
        for (Item item : items) {
            sb.append(item.getName()).append(" ");
        }
        Log.info(sb.toString());
    }
}
