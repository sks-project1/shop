package org.acme;

import java.util.List;

import io.getunleash.DefaultUnleash;
import io.getunleash.Unleash;
import io.getunleash.util.UnleashConfig;
import io.quarkus.logging.Log;
import io.quarkus.rest.client.reactive.QuarkusRestClientBuilder;

import java.net.URI;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.Produces;

import jakarta.inject.Inject;

@Path("/shop")
public class ShopResource {

    private final AssociationService associationService;
    private final AssociationService associationServiceJavaSmart;
    private final AssociationService associationServiceRust;
    private final AssociationService associationServiceGo;

    public ShopResource() {
        this.associationService = QuarkusRestClientBuilder.newBuilder()
            .baseUri(URI.create("http://as-java-v1-service.as-java-v1:8090"))
            .build(AssociationService.class);
        this.associationServiceJavaSmart = QuarkusRestClientBuilder.newBuilder()
            .baseUri(URI.create("http://as-java-service.as-java-v2:8090"))
            .build(AssociationService.class);
        this.associationServiceRust = QuarkusRestClientBuilder.newBuilder()
            .baseUri(URI.create("http://as-rust-service.as-rust-v1:8090"))
            .build(AssociationService.class);
        this.associationServiceGo = QuarkusRestClientBuilder.newBuilder()
            .baseUri(URI.create("http://as-go-service.as-go-v1:8090"))
            .build(AssociationService.class);
    }

    UnleashConfig config = UnleashConfig.builder()
        .appName("my.java-app")
        .instanceId("your-instance-1")
        .unleashAPI("http://unleash-server.unleash:4242/api/")
        .apiKey("default:development.unleash-insecure-api-token")
        .build();

    Unleash unleash = new DefaultUnleash(config);

    @Inject
    Shop shop;

    @GET
    @Path("/toggle")
    @Produces(MediaType.TEXT_PLAIN)
    public String toggle() {
        if (unleash.isEnabled("Toggle")) {
            return "Enabled";
        } else {
            return "Disabled";
        }
    }

    @GET
    @Path("/health")
    @Produces(MediaType.TEXT_PLAIN)
    public String health() {
        return "OK";
    }

    @GET
    @Path("/items")
    public List<Item> list() {
        return shop.getItems();
    }

    @POST
    @Path("/checkout")
    public void checkout(List<Item> cart) {
        if(unleash.isEnabled("Toggle")) {
            StringBuilder sb = new StringBuilder();
            for (Item item : cart) {
                sb.append(item.getName());
                sb.append(" ");
            }
            Log.info("Checkout: " + sb.toString());
            Log.info("Association Service: Enabled");
            if (unleash.isEnabled("Java")) {
                Log.info("Using Apriori Java.");
                associationServiceJavaSmart.checkout(cart);
            } else if (unleash.isEnabled("Rust")) {
                Log.info("Using Apriori Rust.");
                associationServiceRust.checkout(cart);
            } else if (unleash.isEnabled("Go")) {
                Log.info("Using Apriori Go.");
                associationServiceGo.checkout(cart);
            } else {
                Log.info("Using BruteForce Java.");
                associationService.checkout(cart);
            }
        } else {
            Log.info("Association Service: Disabled. Using default.");
            shop.addCart(cart);
        }
    }

    @POST
    @Path("/associations")
    public List<Item> getAssociations(List<Item> items) {
        if(unleash.isEnabled("Toggle")) {
            StringBuilder sb = new StringBuilder();
            for (Item item : items) {
                sb.append(item.getName());
                sb.append(" ");
            }
            Log.info("Find Associations for: " + sb.toString());
            List<Item> result;
            if (unleash.isEnabled("Java")) {
                Log.info("Using Apriori Java.");
                result = associationServiceJavaSmart.getAssociations(items);
            } else if (unleash.isEnabled("Rust")) {
                Log.info("Using Apriori Rust.");
                result = associationServiceRust.getAssociations(items);
            } else if (unleash.isEnabled("Go")) {
                Log.info("Using Apriori Go.");
                result = associationServiceGo.getAssociations(items);
            } else {
                Log.info("Using BruteForce Java.");
                result = associationService.getAssociations(items);
            }
            // create message string with each item name for logging
            sb.setLength(0);
            for (Item item : result) {
                sb.append(item.getName());
                sb.append(" ");
            }
            Log.info("Associations: " + sb.toString());
            return result;
        }
        Log.info("Association Service: Disabled. Using default.");
        return shop.getAssociations(items);
    }
}
