{
  description = "A Nix-flake-based Java development environment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";
  };

  outputs = { self, nixpkgs, pre-commit-hooks }:
    let
      javaVersion = 17; # Change this value to update the whole stack
      system = "x86_64-linux";
      overlays = [
        (final: prev: rec {
          jdk = prev."jdk${toString javaVersion}";
          gradle = prev.gradle.override { java = jdk; };
          maven = prev.maven.override { inherit jdk; };
          quarkus = (import nixpkgs { inherit system; }).stdenv.mkDerivation rec {
            pname = "quarkus";
            version = "3.4.1";
            src = builtins.fetchurl {
              url = "https://github.com/quarkusio/quarkus/releases/download/${version}/quarkus-cli-${version}.tar.gz";
              sha256 = "1731mkpvldpw7r8pyzarmg9fvg1c7x2j0qi91vwh5bjihacf14wc";
            };
            nativeBuildInputs = [ final.autoPatchelfHook ];
            buildInputs = [ jdk maven ];
            sourceRoot = "./quarkus-cli-3.4.1";
            installPhase = ''
              install -m755 -D ./bin/quarkus $out/bin/quarkus
              install -m755 -D ./bin/quarkus.bat $out/bin/quarkus.bat
              install -m755 -D ./lib/quarkus-cli-${version}-runner.jar $out/lib/quarkus-cli-${version}-runner.jar
            '';
          };
        })
      ];
      pkgs = import nixpkgs {
        config.allowUnfree = true;
        inherit overlays system;
      };
      checks = {
        pre-commit-check = pre-commit-hooks.lib.${system}.run {
          src = ./.;
          hooks = {
            nixpkgs-fmt.enable = true;
            verify-project = {
              enable = true;
              name = "verify project";
              entry = ''
                ${pkgs.bash}/bin/bash -c "./mvnw clean"
                ${pkgs.bash}/bin/bash -c "./mvnw package"
                ${pkgs.bash}/bin/bash -c "./mvnw verify"
              '';
              language = "script";
            };
          };
        };
      };
    in
    {
      devShells.${system}.default = pkgs.mkShell {
        packages = with pkgs; [
          age
          fluxcd
          gitlab-runner
          jdk
          k9s
          kind
          kubectx
          kubernetes
          kubernetes-helm
          maven
          pipr
          pre-commit
          quarkus
          nodejs_21
          (
            vscode-with-extensions.override {
              vscodeExtensions = with vscode-extensions; [
                christian-kohler.path-intellisense
                gitlab.gitlab-workflow
                github.copilot
                redhat.java
                redhat.vscode-xml
                redhat.vscode-yaml
                vscjava.vscode-java-debug
                vscjava.vscode-java-test
                vscjava.vscode-maven
                jnoortheen.nix-ide
              ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
                {
                  name = "helm-intellisense";
                  publisher = "tim-koehler";
                  version = "0.14.3";
                  sha256 = "sha256-TcXn8n6mKEFpnP8dyv+nXBjsyfUfJNgdL9iSZwA5eo0=";
                }
                {
                  name = "vscode-gitops-tools";
                  publisher = "Weaveworks";
                  version = "0.25.1698801123";
                  sha256 = "sha256-ceJaj8Hjna3V4ihsOE4CXk+zMD5XWZc42T9J4CrpvR4=";
                }
              ];
            }
          )
        ];
        shellHook =
          checks.pre-commit-check.shellHook +
          ''
            export JAVA_HOME=${pkgs.jdk}/lib/openjdk
          '';
      };
    };
}

